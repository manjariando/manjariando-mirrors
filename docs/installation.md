# Installation

At the command line:

    $ easy_install manjariando-mirrors

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv manjariando-mirrors
    $ pip install manjariando-mirrors
