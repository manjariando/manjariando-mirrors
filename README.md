# pacman-mirrors

[![Build Status](https://travis-ci.org/manjaro/pacman-mirrors.svg?branch=master)](https://travis-ci.org/manjaro/pacman-mirrors)

Package that provides all mirrors for Manjaro Linux.

- Free software: GPL license

## Features

- Generate a new mirror list by using several options:
    - method: rank or random.
    - branch: stable, testing or unstable.
    - country: a single, a list or all.
- A GUI for selecting the mirrors to used to generate a custom list.

## Technologies

pacman-mirrors is build with Python and Gtk3.
