# Translations template for manjariando-mirrors.
# Copyright (C) 2019 ORGANIZATION
# This file is distributed under the same license as the manjariando-mirrors
# project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: manjariando-mirrors 4.14.1\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-04-22 09:20+0200\n"
"PO-Revision-Date: 2020-04-16 03:50-0300\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"
"X-Generator: Poedit 2.3\n"
"Last-Translator: tioguda <guda.flavio@gmail.com>\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Language: pt_BR\n"

#: manjariando_mirrors/constants/txt.py:29
msgid "ERROR"
msgstr "ERRO"

#: manjariando_mirrors/constants/txt.py:30
msgid "INFO"
msgstr "INFO"

#: manjariando_mirrors/constants/txt.py:31
msgid "WARNING"
msgstr "ATENÇÃO"

#: manjariando_mirrors/constants/txt.py:33
msgid "HTTPException"
msgstr "Exceção HTTP"

#: manjariando_mirrors/constants/txt.py:34
msgid "TIMEOUT"
msgstr "TEMPO ESGOTADO"

#: manjariando_mirrors/constants/txt.py:36
msgid "API"
msgstr "API"

#: manjariando_mirrors/constants/txt.py:37
msgid "BRANCH"
msgstr "RAMO"

#: manjariando_mirrors/constants/txt.py:38
msgid "COUNTRY"
msgstr "PAÍS"

#: manjariando_mirrors/constants/txt.py:39
msgid "EXPECTED"
msgstr "ESPERADO"

#: manjariando_mirrors/constants/txt.py:40
msgid "FILE"
msgstr "ARQUIVO"

#: manjariando_mirrors/constants/txt.py:41
msgid "METHOD"
msgstr "MÉTODO"

#: manjariando_mirrors/constants/txt.py:42
msgid "METHODS"
msgstr "MÉTODOS"

#: manjariando_mirrors/constants/txt.py:43
msgid "MISC"
msgstr "DIVERSOS"

#: manjariando_mirrors/constants/txt.py:44
msgid "NUMBER"
msgstr "NÚMERO"

#: manjariando_mirrors/constants/txt.py:45
msgid "PATH"
msgstr "CAMINHO"

#: manjariando_mirrors/constants/txt.py:46
msgid "PREFIX"
msgstr "PREFIXO"

#: manjariando_mirrors/constants/txt.py:47
msgid "PROTO"
msgstr "PROTO"

#: manjariando_mirrors/constants/txt.py:48
msgid "SECONDS"
msgstr "SEGUNDOS"

#: manjariando_mirrors/constants/txt.py:49
#: manjariando_mirrors/constants/txt.py:134
msgid "URL"
msgstr "URL"

#: manjariando_mirrors/constants/txt.py:50
msgid "USAGE"
msgstr "USO"

#: manjariando_mirrors/constants/txt.py:52
msgid "Return branch from configuration"
msgstr "Retornar ramo da configuração"

#: manjariando_mirrors/constants/txt.py:53
msgid "Set prefix to"
msgstr "Defina o prefixo como"

#: manjariando_mirrors/constants/txt.py:54
msgid "Replace protocols in configuration"
msgstr "Substituir protocolos na configuração"

#: manjariando_mirrors/constants/txt.py:55
msgid "Replace branch in mirrorlist"
msgstr "Substituir ramo na lista de espelhos"

#: manjariando_mirrors/constants/txt.py:56
msgid "Replace branch in configuration"
msgstr "Substituir ramo na configuração"

#: manjariando_mirrors/constants/txt.py:57
msgid "Replace mirror url in mirrorlist"
msgstr "Substituir URL do espelho na lista de espelhos"

#: manjariando_mirrors/constants/txt.py:58
msgid "Branch name"
msgstr "Nome do ramo"

#: manjariando_mirrors/constants/txt.py:59
msgid "Comma separated list of countries, from which mirrors will be used"
msgstr "Lista de países separados por vírgula, a partir da qual os espelhos serão usados"

#: manjariando_mirrors/constants/txt.py:61
msgid "Load default mirror file"
msgstr "Carregar arquivo espelho padrão"

#: manjariando_mirrors/constants/txt.py:62
msgid "Generate mirrorlist with a number of up-to-date mirrors. Overrides"
msgstr "Gerar lista de espelhos com um número de espelhos atualizados. Substituições"

#: manjariando_mirrors/constants/txt.py:64
msgid "Generate mirrorlist with defaults"
msgstr "Gerar lista de espelhos com padrões"

#: manjariando_mirrors/constants/txt.py:65
msgid "Get current country using geolocation"
msgstr "Obter o país atual usando geolocalização"

#: manjariando_mirrors/constants/txt.py:66
msgid "List all available countries"
msgstr "Listar todos os países disponíveis"

#: manjariando_mirrors/constants/txt.py:67
msgid "Generate custom mirrorlist"
msgstr "Gerar lista de espelhos personalizada"

#: manjariando_mirrors/constants/txt.py:68
msgid "Generation method"
msgstr "Método de geração"

#: manjariando_mirrors/constants/txt.py:69
msgid "Use to skip generation of mirrorlist"
msgstr "Use para pular a geração da lista de espelhos"

#: manjariando_mirrors/constants/txt.py:70
msgid "Quiet mode - less verbose output"
msgstr "Modo silencioso - saída menos detalhada"

#: manjariando_mirrors/constants/txt.py:71
msgid "Ignore mirror branch status"
msgstr "Ignorar status de ramo de espelho"

#: manjariando_mirrors/constants/txt.py:72
msgid "Syncronize manjariando databases"
msgstr "Sincronizar banco de dados Manjariando"

#: manjariando_mirrors/constants/txt.py:73
msgid "Maximum waiting time for server response"
msgstr "Tempo máximo de espera para resposta do servidor"

#: manjariando_mirrors/constants/txt.py:74
msgid "Print the manjariando-mirrors version"
msgstr "Imprimir a versão de manjariando-mirrors"

#: manjariando_mirrors/constants/txt.py:76
msgid "Branch in config is changed"
msgstr "A ramo na configuração foi alterada"

#: manjariando_mirrors/constants/txt.py:77
msgid "Protocols in config is changed"
msgstr "Protocolos na configuração alterados"

#: manjariando_mirrors/constants/txt.py:78
msgid "Re-branch requires a branch to work with"
msgstr "Re-ramo requer um ramo de trabalho com"

#: manjariando_mirrors/constants/txt.py:79
msgid "Branch in mirror list is changed"
msgstr "A ramo na lista de espelhos foi alterada"

#: manjariando_mirrors/constants/txt.py:80
msgid "Available countries are"
msgstr "Os países disponíveis são"

#: manjariando_mirrors/constants/txt.py:81
msgid "Could not download from"
msgstr "Não foi possível fazer o download de"

#: manjariando_mirrors/constants/txt.py:82
msgid "Cannot read file"
msgstr "Não é possível ler o arquivo"

#: manjariando_mirrors/constants/txt.py:83
msgid "Cannot write file"
msgstr "Não é possível gravar o arquivo"

#: manjariando_mirrors/constants/txt.py:84
msgid "Converting custom mirror file to new format"
msgstr "Convertendo arquivo do espelho personalizado para novo formato"

#: manjariando_mirrors/constants/txt.py:85
msgid "Custom mirror file"
msgstr "Arquivo do espelho personalizado"

#: manjariando_mirrors/constants/txt.py:86
msgid "Custom mirror file saved"
msgstr "Arquivo do espelho personalizado salvo"

#: manjariando_mirrors/constants/txt.py:87
msgid "User generated mirror list"
msgstr "Lista de espelhos gerada pelo usuário"

#: manjariando_mirrors/constants/txt.py:88
msgid "The custom pool is empty"
msgstr "O pool personalizado está vazio"

#: manjariando_mirrors/constants/txt.py:89
msgid "The default pool is empty"
msgstr "O pool padrão está vazio"

#: manjariando_mirrors/constants/txt.py:90
msgid "Deprecated argument"
msgstr "Argumento descontinuado"

#: manjariando_mirrors/constants/txt.py:91
msgid "doesn't exist."
msgstr "não existe."

#: manjariando_mirrors/constants/txt.py:92
msgid "Downloading mirrors from"
msgstr "Baixando espelhos de"

#: manjariando_mirrors/constants/txt.py:93
msgid "Falling back to"
msgstr "Voltando a"

#: manjariando_mirrors/constants/txt.py:94
msgid "Internet connection appears to be down"
msgstr "A conexão com a Internet parece estar inativa"

#: manjariando_mirrors/constants/txt.py:95
msgid "Mirror list is generated using random method"
msgstr "A lista de espelhos é gerada usando o método aleatório"

#: manjariando_mirrors/constants/txt.py:96
msgid "Invalid setting in"
msgstr "Configuração inválida em"

#: manjariando_mirrors/constants/txt.py:97
msgid "is missing"
msgstr "está faltando"

#: manjariando_mirrors/constants/txt.py:98
msgid "The mirror file"
msgstr "O arquivo do espelho"

#: manjariando_mirrors/constants/txt.py:99
msgid "Mirror list generated and saved to"
msgstr "Lista de espelhos gerada e salva em"

#: manjariando_mirrors/constants/txt.py:100
msgid "Generated on"
msgstr "Gerado em"

#: manjariando_mirrors/constants/txt.py:101
msgid "custom mirrorlist"
msgstr "lista de espelho personalizada"

#: manjariando_mirrors/constants/txt.py:102
msgid "To reset custom mirrorlist"
msgstr "Para redefinir a lista de espelho personalizada"

#: manjariando_mirrors/constants/txt.py:103
msgid "default mirrorlist"
msgstr "lista de espelhos padrão"

#: manjariando_mirrors/constants/txt.py:104
msgid "to modify mirrorlist"
msgstr "modificar lista de espelhos"

#: manjariando_mirrors/constants/txt.py:105
msgid "The mirror pool is empty"
msgstr "O conjunto de espelhos está vazio"

#: manjariando_mirrors/constants/txt.py:106
msgid "Mirror ranking is not available"
msgstr "A classificação espelhada não está disponível"

#: manjariando_mirrors/constants/txt.py:107
msgid "argument is missing"
msgstr "argumento está faltando"

#: manjariando_mirrors/constants/txt.py:108
msgid "Must have root privileges"
msgstr "Deve ter privilégios de root"

#: manjariando_mirrors/constants/txt.py:109
msgid "The mirrors has not changed"
msgstr "Os espelhos não mudaram"

#: manjariando_mirrors/constants/txt.py:110
msgid "No mirrors in selection"
msgstr "Sem espelhos na seleção"

#: manjariando_mirrors/constants/txt.py:111
msgid "Option"
msgstr "Opção"

#: manjariando_mirrors/constants/txt.py:112
msgid "You have chosen to override the status for your system branch."
msgstr "Você optou por substituir o status da sua ramificação do sistema."

#: manjariando_mirrors/constants/txt.py:113
msgid "Servers in the mirror list might not be up-to-date."
msgstr "Os servidores na lista de espelhos podem não estar atualizados."

#: manjariando_mirrors/constants/txt.py:114
msgid "Please use"
msgstr "Por favor, use"

#: manjariando_mirrors/constants/txt.py:115
msgid "Querying mirrors"
msgstr "Consultando espelhos"

#: manjariando_mirrors/constants/txt.py:116
msgid "Randomizing mirror list"
msgstr "Lista aleatória de espelhos"

#: manjariando_mirrors/constants/txt.py:117
msgid "To remove custom config run "
msgstr "Para remover a configuração personalizada, execute "

#: manjariando_mirrors/constants/txt.py:118
msgid "This may take some time"
msgstr "Isto pode levar algum tempo"

#: manjariando_mirrors/constants/txt.py:119
msgid "unknown country"
msgstr "país desconhecido"

#: manjariando_mirrors/constants/txt.py:120
msgid "Use 0 for all mirrors"
msgstr "Use 0 para todos os espelhos"

#: manjariando_mirrors/constants/txt.py:121
msgid "Using all mirrors"
msgstr "Usando todos os espelhos"

#: manjariando_mirrors/constants/txt.py:122
msgid "Using custom mirror file"
msgstr "Usando o arquivo personalizado de espelho"

#: manjariando_mirrors/constants/txt.py:123
msgid "Using default mirror file"
msgstr "Usando o arquivo padrão de espelho"

#: manjariando_mirrors/constants/txt.py:124
msgid "Writing mirror list"
msgstr "Escrevendo lista de espelhos"

#: manjariando_mirrors/constants/txt.py:127
msgid "Manjariando mirrors by response time"
msgstr "Espelhos Manjariando por tempo de resposta"

#: manjariando_mirrors/constants/txt.py:128
msgid "Manjariando mirrors in random order"
msgstr "Espelhos do Manjariando em ordem aleatória"

#: manjariando_mirrors/constants/txt.py:129
msgid "Check mirrors for your personal list"
msgstr "Verifique espelhos para sua lista pessoal"

#: manjariando_mirrors/constants/txt.py:130
msgid "Use"
msgstr "Usar"

#: manjariando_mirrors/constants/txt.py:131
msgid "Country"
msgstr "País"

#: manjariando_mirrors/constants/txt.py:132
msgid "Resp"
msgstr "Resposta"

#: manjariando_mirrors/constants/txt.py:133
msgid "Sync"
msgstr "Sincronizado"

#: manjariando_mirrors/constants/txt.py:135
msgid "Cancel"
msgstr "Cancelar"

#: manjariando_mirrors/constants/txt.py:136
msgid "OK"
msgstr "Ok"

#: manjariando_mirrors/constants/txt.py:137
msgid "Confirm selections"
msgstr "Confirmar seleções"

#: manjariando_mirrors/constants/txt.py:138
msgid "I want to use these mirrors"
msgstr "Eu quero usar esses espelhos"

#: manjariando_mirrors/constants/txt.py:139
msgid "Mirrorlist generated and saved to"
msgstr "Lista de espelhos gerada e salva em"

#: manjariando_mirrors/constants/txt.py:140
msgid "Querying servers, this may take some time"
msgstr "Consultando servidores, isso pode levar algum tempo"

#: manjariando_mirrors/constants/txt.py:141
msgid "Testing mirrors in"
msgstr "Testando espelhos no(a)"

#: manjariando_mirrors/constants/txt.py:142
msgid "Don't generate mirrorlist if"
msgstr "Não gere lista de espelhos se"

#: manjariando_mirrors/constants/txt.py:143
msgid "in the configuration file"
msgstr "no arquivo de configuração"

#: manjariando_mirrors/constants/txt.py:144
msgid "Output file"
msgstr "Arquivo de saída"

#: manjariando_mirrors/constants/txt.py:145
msgid "Mirrors list path"
msgstr "Caminho da lista de espelhos"

#: manjariando_mirrors/constants/txt.py:146
msgid "Get current country using geolocation. Ignored if"
msgstr "Obtenha o país atual usando geolocalização. Ignorado se"

#: manjariando_mirrors/constants/txt.py:147
msgid "is supplied"
msgstr "é fornecida"

#: manjariando_mirrors/constants/txt.py:148
msgid "Generate mirrorlist"
msgstr "Gerar lista de espelhos"
