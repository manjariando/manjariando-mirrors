# Credits

## Development Lead
- Roland Singer <roland@manjaro.org>
- Esclapion
- philm
- Ramon Buldó <rbuldo@gmail.com>
- Hugo Posnic <huluti@manjaro.org>

## Contributors
- Frede Hundewadt

Thank you all! ;)
